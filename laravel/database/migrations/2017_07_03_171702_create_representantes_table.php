<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesTable extends Migration
{
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('estado');
            $table->string('cidade');
            $table->string('telefone');
            $table->text('endereco');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('representantes');
    }
}
