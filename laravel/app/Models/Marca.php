<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Marca extends Model
{
    protected $table = 'marcas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'   => 230,
            'height'  => 180,
            'bgcolor' => '#fff',
            'path'    => 'assets/img/marcas/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 625,
            'height' => 440,
            'path'   => 'assets/img/marcas/'
        ]);
    }
}
