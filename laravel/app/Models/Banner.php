<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 1200,
            'height' => 445,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'  => 300,
            'height' => 220,
            'path'   => 'assets/img/banners/'
        ]);
    }
}
