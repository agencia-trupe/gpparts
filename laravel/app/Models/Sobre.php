<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sobre extends Model
{
    protected $table = 'sobre';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 960,
            'height' => 595,
            'path'   => 'assets/img/sobre/'
        ]);
    }

}
