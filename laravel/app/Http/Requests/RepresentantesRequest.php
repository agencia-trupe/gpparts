<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RepresentantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'estado' => 'required',
            'cidade' => 'required',
            'telefone' => 'required',
            'endereco' => 'required',
            'link' => '',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
