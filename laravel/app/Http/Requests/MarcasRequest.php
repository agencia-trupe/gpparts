<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MarcasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'marca' => 'required|image',
            'imagem' => 'required|image',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
