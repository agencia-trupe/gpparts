<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MarcasBannersRequest;
use App\Http\Controllers\Controller;

use App\Models\MarcaBanner;

class MarcasBannersController extends Controller
{
    public function index()
    {
        $registros = MarcaBanner::ordenados()->get();

        return view('painel.marcas-banners.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.marcas-banners.create');
    }

    public function store(MarcasBannersRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = MarcaBanner::upload_imagem();

            MarcaBanner::create($input);

            return redirect()->route('painel.marcas-banners.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(MarcaBanner $registro)
    {
        return view('painel.marcas-banners.edit', compact('registro'));
    }

    public function update(MarcasBannersRequest $request, MarcaBanner $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = MarcaBanner::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.marcas-banners.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(MarcaBanner $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.marcas-banners.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
