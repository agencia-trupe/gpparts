<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RepresentantesRequest;
use App\Http\Controllers\Controller;

use App\Models\Representante;
use App\Models\ContatoRepresentante;

class RepresentantesController extends Controller
{
    public function index()
    {
        $registros = Representante::ordenados()->get();
        $contatosRepresentantesNaoLidos = ContatoRepresentante::naoLidos()->count();

        return view('painel.representantes.index', compact('registros', 'contatosRepresentantesNaoLidos'));
    }

    public function create()
    {
        return view('painel.representantes.create');
    }

    public function store(RepresentantesRequest $request)
    {
        try {

            $input = $request->all();

            Representante::create($input);

            return redirect()->route('painel.representantes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Representante $registro)
    {
        return view('painel.representantes.edit', compact('registro'));
    }

    public function update(RepresentantesRequest $request, Representante $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.representantes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Representante $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.representantes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
