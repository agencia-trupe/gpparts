<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRepresentante;

class RepresentantesContatosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ContatoRepresentante::orderBy('id', 'DESC')->paginate(15);

        return view('painel.representantes.contatos.index', compact('contatosrecebidos'));
    }

    public function show(ContatoRepresentante $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.representantes.contatos.show', compact('contato'));
    }

    public function destroy(ContatoRepresentante $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.representantes.contatos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
