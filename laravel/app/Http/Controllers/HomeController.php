<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Marca;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $marcas  = Marca::ordenados()->get();

        return view('frontend.home', compact('banners', 'marcas'));
    }
}
