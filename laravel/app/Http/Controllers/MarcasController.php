<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Marca;
use App\Models\MarcaBanner;

class MarcasController extends Controller
{
    public function index()
    {
        $marcas  = Marca::ordenados()->get();
        $banners = MarcaBanner::ordenados()->get();

        return view('frontend.marcas', compact('marcas', 'banners'));
    }
}
