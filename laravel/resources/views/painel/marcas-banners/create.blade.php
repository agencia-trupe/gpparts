@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcas / Banners /</small> Adicionar Banner</h2>
    </legend>

    {!! Form::open(['route' => 'painel.marcas-banners.store', 'files' => true]) !!}

        @include('painel.marcas-banners.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
