@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Marcas / Banners /</small> Editar Banner</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.marcas-banners.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.marcas-banners.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
