@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (960x595px)') !!}
    @if($contato->imagem)
    <img src="{{ url('assets/img/contato/'.$contato->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('twitter', 'Twitter') !!}
    {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'Linkedin') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
