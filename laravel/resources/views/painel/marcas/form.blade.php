@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('marca', 'Marca') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/marcas/'.$registro->marca) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('marca', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem (625x440px)') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/marcas/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'marca']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.marcas.index') }}" class="btn btn-default btn-voltar">Voltar</a>
