@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Representantes /</small> Editar Representante</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.representantes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.representantes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
