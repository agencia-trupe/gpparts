@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Representantes
            <div class="btn-group pull-right">
                <a href="{{ route('painel.representantes.contatos.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-envelope" style="margin-right:10px;"></span>
                    Contatos Recebidos
                    @if($contatosRepresentantesNaoLidos >= 1)
                    <span class="label label-warning" style="margin-left:3px;">{{ $contatosRepresentantesNaoLidos }}</span>
                    @endif
                </a>
                <a href="{{ route('painel.representantes.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Representante</a>
            </div>
        </h2>
    </legend>


    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="representantes">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Estado</th>
                <th>Cidade</th>
                <th>Nome</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->estado }}</td>
                <td>{{ $registro->cidade }}</td>
                <td>{{ $registro->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.representantes.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.representantes.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
