<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.marcas*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.marcas.index') }}">Marcas</a>
    </li>
    <li @if(str_is('painel.representantes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.representantes.index') }}">
            Representantes
            @if($contatosRepresentantesNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosRepresentantesNaoLidos }}</span>
            @endif
        </a>
    </li>
	<li @if(str_is('painel.sobre*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.sobre.index') }}">Sobre</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
