@extends('frontend.common.template')

@section('content')

    <div class="representantes">
        <div class="encontre">
            <div class="center">
                <h2>ENCONTRE O REPRESENTANTE MAIS PRÓXIMO DE VOCÊ</h2>
                <div class="select">{!! Form::select('estado', $estados, request('uf'), ['placeholder' => 'ESTADO &middot; UF (Selecione)']) !!}</div>
                <div class="select">{!! Form::select('cidade', $cidades, request('cidade'), ['placeholder' => 'CIDADE (Selecione)']) !!}</div>

                <div class="representantes-show">
                    @foreach($representantes as $representante)
                    <div class="representante">
                        <p>
                            {{ $representante->nome }}<br>
                            <span class="telefone">{{ $representante->telefone }}</span><br>
                            {!! $representante->endereco !!}
                            <a href="http://{{ $representante->link }}" target="_blank">{{ $representante->link }}</a>
                        </p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
