@extends('frontend.common.template')

@section('content')

    <div class="representantes">
        <div class="encontre">
            <div class="center">
                <h2>ENCONTRE O REPRESENTANTE MAIS PRÓXIMO DE VOCÊ</h2>
                <div class="select">{!! Form::select('estado', $estados, request('uf'), ['placeholder' => 'ESTADO &middot; UF (Selecione)']) !!}</div>
                <div class="select">{!! Form::select('cidade', [], request('cidade'), ['placeholder' => 'CIDADE (Selecione)', 'disabled' => true]) !!}</div>
            </div>
        </div>

        <form action="" id="form-representante" method="POST">
            <div class="center">
                <h2>CADASTRE-SE PARA SER UM DE NOSSOS REPRESENTANTES</h2>
                <div class="input">
                    <label for="nome">nome</label>
                    <input type="text" name="nome" id="nome" required>
                </div>
                <div class="input">
                    <label for="email">e-mail</label>
                    <input type="email" name="email" id="email" required>
                </div>
                <div class="input">
                    <label for="telefone">telefone</label>
                    <input type="text" name="telefone" id="telefone">
                </div>
                <div class="input">
                    <label for="cidade_uf">cidade / estado</label>
                    <input type="text" name="cidade_uf" id="cidade_uf">
                </div>
                <div class="input">
                    <label for="mensagem">mensagem</label>
                    <textarea name="mensagem" id="mensagem" required></textarea>
                </div>
                <div class="input">
                    <div id="form-representantes-response"></div>
                    <input type="submit" value="ENVIAR">
                </div>
            </div>
        </form>
    </div>

@endsection
