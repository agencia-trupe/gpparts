@extends('frontend.common.template')

@section('content')

    <style>.slick-list{height:auto !important;}</style>

    <div class="home-banners">
        <div class="center">
            <div class="banners-wrapper">
                @foreach($banners as $banner)
                    @if($banner->link)
                    <a href="{{ $banner->link }}" class="banner">
                        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                        <img src="{{ asset('assets/img/banners/'.$banner->marca) }}" alt="">
                    </a>
                    @else
                    <div class="banner">
                        <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                        <img src="{{ asset('assets/img/banners/'.$banner->marca) }}" alt="">
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div class="home-marcas">
        <div class="center">
            <div class="marcas-wrapper">
                @foreach($marcas as $marca)
                <img src="{{ asset('assets/img/marcas/'.$marca->marca) }}" alt="">
                @endforeach
            </div>
        </div>
    </div>

@endsection
