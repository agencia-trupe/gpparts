    <footer>
        <div class="footer-main">
            <div class="center">
                <div class="col">
                    <a href="{{ route('home') }}">HOME</a>
                    <a href="{{ route('produtos') }}">A GP PARTS</a>
                    <a href="{{ route('informacoes-tecnicas') }}">INFORMAÇÕES TÉCNICAS</a>
                    <a href="{{ route('contato') }}">CONTATO</a>
                </div>
                <div class="col">
                    <a href="{{ route('produtos') }}">PRODUTOS</a>
                    <a href="{{ route('marcas') }}">MARCAS</a>
                    <a href="{{ route('onde-encontrar') }}">ONDE ENCONTRAR</a>
                </div>
                <div class="col">
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                    <a href="{{ route('home') }}">CATEGORIA EXEMPLO</a>
                </div>
                <div class="col-form">
                    <img src="{{ asset('assets/img/layout/marca-gpparts-rodape.png') }}" alt="">
                    <form action="" id="form-contato-footer" method="POST">
                        <p>{{ $contato->telefone }}</p>
                        <div class="field-group">
                            <input type="text" name="nome" id="footer-nome" placeholder="nome" required>
                            <input type="email" name="email" id="footer-email" placeholder="e-mail" required>
                            <input type="text" name="telefone" id="footer-telefone" placeholder="telefone">
                        </div>
                        <textarea name="mensagem" id="footer-mensagem" placeholder="mensagem" required></textarea>
                        <input type="submit" value="ENVIAR">
                        <div id="form-contato-footer-response">Preencha todos os campos corretamente</div>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados.
                    <span>|</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
