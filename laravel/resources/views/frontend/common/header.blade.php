    <header>
        <div class="center">
            <div class="top-nav">
                <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
                <a href="{{ route('a-gp-parts') }}" @if(Tools::isActive('a-gp-parts')) class="active" @endif>A GP PARTS</a>
                <a href="{{ route('informacoes-tecnicas') }}" @if(Tools::isActive('informacoes-tecnicas')) class="active" @endif>INFORMAÇÕES TÉCNICAS</a>
                <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
                <div class="social">
                    @foreach(['facebook', 'twitter', 'linkedin'] as $s)
                    <a href="{{ $contato->{$s} }}" class="{{ $s }}">{{ $s }}</a>
                    @endforeach
                </div>
            </div>
            <div class="main-nav">
                <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
                <a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif><span>PRODUTOS</span></a>
                <a href="{{ route('marcas') }}" @if(Tools::isActive('marcas*')) class="active" @endif><span>MARCAS</span></a>
                <a href="{{ route('onde-encontrar') }}" @if(Tools::isActive('onde-encontrar*')) class="active" @endif><span>ONDE ENCONTRAR</span></a>
                <a href="#" class="busca-direta-handle"><span>BUSCA DIRETA</span></a>
                <a href="#" class="busca-avancada-handle"><span>BUSCA AVANÇADA</span></a>
                <div id="mobile-toggle">
                    <button type="button" role="button">
                        <span class="lines"></span>
                    </button>
                </div>
            </div>
        </div>
    </header>
    <div id="nav-mobile">
        <a href="{{ route('home') }}" @if(Tools::isActive('home')) class="active" @endif>HOME</a>
        <a href="{{ route('produtos') }}" @if(Tools::isActive('produtos*')) class="active" @endif><span>PRODUTOS</span></a>
        <a href="{{ route('marcas') }}" @if(Tools::isActive('marcas*')) class="active" @endif><span>MARCAS</span></a>
        <a href="{{ route('onde-encontrar') }}" @if(Tools::isActive('onde-encontrar*')) class="active" @endif><span>ONDE ENCONTRAR</span></a>
        <a href="{{ route('a-gp-parts') }}" @if(Tools::isActive('a-gp-parts')) class="active" @endif>A GP PARTS</a>
        <a href="{{ route('informacoes-tecnicas') }}" @if(Tools::isActive('informacoes-tecnicas')) class="active" @endif>INFORMAÇÕES TÉCNICAS</a>
        <a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
        <a href="#" class="busca-direta-handle"><span>BUSCA DIRETA</span></a>
        <a href="#" class="busca-avancada-handle"><span>BUSCA AVANÇADA</span></a>
        <div class="social">
            @foreach(['facebook', 'twitter', 'linkedin'] as $s)
            <a href="{{ $contato->{$s} }}" class="{{ $s }}">{{ $s }}</a>
            @endforeach
        </div>
    </div>
