@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
            <div class="texto">
                <h2>FALE CONOSCO</h2>
                <p>{{ $contato->telefone }}</p>
                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="nome" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone">
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                    <div id="form-contato-response">Preencha todos os campos corretamente</div>
                    <input type="submit" value="ENVIAR">
                </form>
            </div>
        </div>
    </div>

@endsection
