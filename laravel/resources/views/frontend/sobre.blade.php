@extends('frontend.common.template')

@section('content')

    <div class="sobre">
        <div class="center">
            <img src="{{ asset('assets/img/sobre/'.$sobre->imagem) }}" alt="">
            <div class="texto">
                <h2>A GP PARTS</h2>
                {!! $sobre->texto !!}
            </div>
        </div>
    </div>

@endsection
