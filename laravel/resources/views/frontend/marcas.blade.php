@extends('frontend.common.template')

@section('content')

    <div class="marcas">
        <div class="center">
            <h2>LOREM IPSUM DOLOR SIT AMET</h2>
        </div>
        @foreach($marcas->chunk(5) as $chunk)
            <div class="row">
                <div class="marcas-thumbs center">
                    @foreach($chunk as $marca)
                    <a href="#" data-imagem="{{ asset('assets/img/marcas/'.$marca->imagem) }}" data-texto="<h3>Sobre a {{ $marca->nome }}</h3>{{ $marca->texto }}">
                        <img src="{{ asset('assets/img/marcas/'.$marca->marca) }}" alt="">
                    </a>
                    @endforeach
                </div>
                <div class="detalhes">
                    <div class="center">
                        <div class="seta"></div>
                        <div class="imagem"></div>
                        <div class="texto"></div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="marcas-banner">
        <div class="banners-wrapper">
            @foreach($banners as $banner)
            <img src="{{ asset('assets/img/marcas-banners/'.$banner->imagem) }}" alt="" class="banner">
            @endforeach
        </div>
    </div>

@endsection
