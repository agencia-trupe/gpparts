import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';
import FormularioContato from './FormularioContato';
import Home from './Home';
import Representantes from './Representantes';
import Marcas from './Marcas';

AjaxSetup();
MobileToggle();
FormularioContato();
Home();
Representantes();
Marcas();
