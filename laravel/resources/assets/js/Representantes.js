export default function Representantes() {
    $('select[name=estado]').change(function() {
        var uf    = $(this).val(),
            base  = $('base').attr('href');

        if (uf) {
            window.location = base + '/onde-encontrar?uf=' + uf;
        } else {
            window.location = base + '/onde-encontrar';
        }
    });

    $('select[name=cidade]').change(function() {
        var uf     = $('select[name=estado]').val(),
            cidade = $(this).val(),
            base   = $('base').attr('href');

        if (uf && cidade) {
            window.location = base + '/onde-encontrar?uf=' + uf + '&cidade=' + cidade;
        } else if (uf) {
            window.location = base + '/onde-encontrar?uf=' + uf;
        } else {
            window.location = base + '/onde-encontrar';
        }
    });

    var envioRepresentantes = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-representantes-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/onde-encontrar',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                cidade_uf: $('#cidade_uf').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente').fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    $('#form-representante').on('submit', envioRepresentantes);
};
