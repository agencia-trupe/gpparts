export default function Marcas() {
    $('.marcas-thumbs a').click( function(e){
        e.preventDefault();

        var $this = $(this);

        if ($this.hasClass('active')) return;

        $('.marcas-thumbs a').removeClass('active');
        $('.detalhes').slideUp().removeClass('active');
        $this.addClass('active');

        var imagem   = $this.data('imagem'),
            texto    = $this.data('texto'),
            detalhes = $this.parent().next('.detalhes');

        var deslocamento = $(this).position().left + ($(this).width() / 2) - (parseInt($('.seta').css('width')) / 2);

        $('.detalhes .seta').css('left', deslocamento);

        var img = new Image();
        img.onload = function() {
            detalhes.find('.imagem').html(`<img src="${imagem}">`);
            detalhes.find('.texto').html(texto);
            detalhes.slideDown(function() {
                $(this).addClass('active');
            });
        }
        img.src = imagem;
    });

    $(window).resize(function() {
        var item = $('.marcas-thumbs a.active');
        if (item.length) {
            var deslocamento = item.position().left + (item.width() / 2) - (parseInt($('.seta').css('width')) / 2);
            $('.detalhes .seta').css('left', deslocamento);
        }
    });
};
